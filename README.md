# Simple Node.js REST API for MSSQL  
Node Express application that creates REST endpoints from MSSQL stored procedures.  
  
## Usage  
// returns a list of objects without parameters  
GET: (endpoint ip)/q/(procedure name)  
  
// returns a list of objects with specified parameters  
POST: (endpoint ip)/q/(procedure name)  
  
// creates a new object  
PUT: (endpoint ip)/q/(procedure name)  
  
// returns a single object with a specified ID  
GET: (endpoint ip)/q/(procedure name)/(object id)  
  
// updates a single object with a specified ID  
PUT: (endpoint ip)/q/(procedure name)/(object id)  
  
// deletes an object with a specified ID  
DELETE: (endpoint ip)/q/(procedure name)/(object id)
