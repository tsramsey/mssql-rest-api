require('dotenv').config();

var express = require('express'),
  app = express(),
  port = 3000,
  bodyParser = require('body-parser'),
  cors = require('cors');

// global mssql instance object
mssql = require('./mssql.js');

app.use(cors());

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var routes = require('./api/routes/routes');
routes(app);

// Basic error handling
app.use(function(req, res) {
  res.status(404).send({url: req.originalUrl + ' not found'});
  res.status(500).send({error: res.error});
});

// load procedures into memory
mssql.getProcedures();

// if DEV, check every (x) minutes for new procedures
if(process.env.DEV) {
  var interval = 1;

  setInterval(function() {
    mssql.getProcedures();
  }, (1000 * 60 * interval));
}

app.listen(port);
console.log('REST server started on: ' + port);