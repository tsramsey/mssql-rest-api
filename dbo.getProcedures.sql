SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Scott Ramsey
-- Create date: 03/12/2018
-- Description:	Returns a list of stored procedures
--				in a specified schema.
-- =============================================
CREATE PROCEDURE [dbo].[getProcedures] 

AS
BEGIN
	SET NOCOUNT ON;

    SELECT
		r.SPECIFIC_SCHEMA + '.' + r.SPECIFIC_NAME AS 'name',
		STUFF
		(
			(
				SELECT
					REPLACE(p.PARAMETER_NAME, '@', '') AS [name]
					,p.DATA_TYPE AS [type]
				FROM INFORMATION_SCHEMA.PARAMETERS p
				WHERE r.SPECIFIC_SCHEMA = p.SPECIFIC_SCHEMA
				AND p.SPECIFIC_NAME = r.SPECIFIC_NAME
				ORDER BY p.ORDINAL_POSITION
				FOR JSON AUTO
			), 1, 1, '['
		) AS 'params'
	FROM INFORMATION_SCHEMA.ROUTINES r
	WHERE r.ROUTINE_TYPE = 'PROCEDURE'

	-- Specify schemas to include here
	AND r.SPECIFIC_SCHEMA IN ('dbo')

	-- Exclude specific procedures here
	AND r.SPECIFIC_NAME NOT IN ('getProcedures')
	ORDER BY
		1
	;
END
