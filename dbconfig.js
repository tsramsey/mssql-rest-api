'use strict';

var config = {  
    userName: process.env.DB_USER,  
    password: process.env.DB_PASS,  
    server: process.env.DB_HOST,
    options: {
        database: process.env.DB_DATABASE,
        rowCollectionOnDone: true,
        rowCollectionOnRequestCompletion: true,
        requestTimeout: 0,
        encrypt: true
    }
};

module.exports = config;