'use strict';

var Model = require('../models/model');  

exports.list = function(req, res) {
    Model.list(req.params.q, req.body, 
        function(err, rowCount, rows) {       
            callback(res, err, rowCount, rows);
        }
    );
};

exports.get = function(req, res) {
    Model.get(req.params.q, req.params.id, 
        function(err, rowCount, rows) {       
            callback(res, err, rowCount, rows);
        }
    );
};

exports.create = function(req, res) {
    Model.create(req.params.q, req.body, 
        function(err, rowCount, rows) {      
            callback(res, err, rowCount, rows);
        }
    );
};

exports.update = function(req, res) {
    Model.update(req.params.q, req.params.id, req.body, 
        function(err, rowCount, rows) {       
            callback(res, err, rowCount, rows);
        }
    );
};

exports.delete = function(req, res) {
    Model.delete(req.params.q, req.params.id, 
        function(err, rowCount, rows) {       
            callback(res, err, rowCount, rows);
        }
    );
};

function callback(res, err, rowCount, rows) {
    if(err) {
        res.json({ error: err.message });
    }
    else {
        var result = [];

        rows.forEach(function(cols) {
            var object = {};

            cols.forEach(function(c) {
                object[ c.metadata.colName ] = _tryParse(c.value);
            });

            result.push(object);
        });

        try {
            res.json(result);
        }
        catch(e) {
            console.log(e);
        }
    }
}

function _tryParse(obj) {
    try {
        var retValue = JSON.parse(obj);
        return retValue;
    }
    catch(e) {
        return obj;
    }
}