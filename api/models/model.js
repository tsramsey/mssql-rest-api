'use strict';

var Connection = require('tedious').Connection;
var Request = require('tedious').Request;
var Types = require('tedious').TYPES;
var config = require('../../dbconfig');

var Model = {  
    list: function(q, model, callback) { 
        return this._execProcedure(q, null, model, callback); 
    },  

    get: function(q, id, callback) {  
        return this._execProcedure(q, id, null, callback);  
    }, 

    create: function(q, model, callback) {   
        return this._execProcedure(q, null, model, callback);  
    },  

    update: function(q, id, model, callback) {
        return this._execProcedure(q, id, model, callback); 
    },

    delete: function(q, id, callback) {  
        return this._execProcedure(q, id, null, callback);   
    },

    _execProcedure: function(q, id, model, callback) {
        if(mssql.procedures[q]) {
            var conn = new Connection(config);

            var params = this._getParams(q, id, model);
            
            conn.on('connect', function(err) {   
                if(err) {
                    callback(err);
                }
                else {
                    var request = new Request(q, function(err, rowCount, rows) {
                        callback(err, rowCount, rows);
                        conn.close();
                    });

                    for(var p in params) {
                        request.addParameter(params[p].name, params[p].type, params[p].value);
                    }
            
                    conn.callProcedure(request);
                }
            });
        }
        else {
            callback({message: q + " does not exist"});
        }
    },

    _getParams: function(q, id, model) {
        var params = [];
    
        if(model == null && id != null) {
            model = {
                "id": id
            }
        }
    
        if(!!model && mssql.procedures[q]) {
            for(var i = 0; i < mssql.procedures[q].length; i++) {
                if(model[mssql.procedures[q][i].name]) {
                    params.push({
                        name: mssql.procedures[q][i].name,
                        type: this._getType(mssql.procedures[q][i].type),
                        value: this._isObject(model[mssql.procedures[q][i].name]) ? 
                            JSON.stringify(model[mssql.procedures[q][i].name]) : model[mssql.procedures[q][i].name]
                    })
                }
            }
        }
    
        return params;
    },

    _isObject: function(obj) {
        return obj === Object(obj);
    },

    /*
     *
     * Update this function to handle additional SQL data types
     * 
     */
    _getType: function(type) {
        switch(type) {
            case 'date':
                return Types.Date;

            case 'datetime':
                return Types.DateTime;

            case 'int':
                return Types.Int;

            case 'float':
                return Types.Float;

            case 'decimal':
                return Types.Decimal;

            case 'nvarchar':
                return Types.NVarChar;

            case 'varchar':
                return Types.VarChar;

            case 'uniqueidentifier':
                return Types.UniqueIdentifier;

            case 'bit':
                return Types.Bit;

            default:
                return Types.NVarChar;
        }
    }
};

module.exports = Model;