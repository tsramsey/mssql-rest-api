'use strict';

module.exports = function(app) {
  var model = require('../controllers/controller');

  app.route('/q/:q')
    .get(model.list)
    .put(model.create)
    .post(model.list);

  app.route('/q/:q/:id')
    .get(model.get)
    .put(model.update)
    .delete(model.delete);
};