'use strict';

var Connection = require('tedious').Connection;
var Request = require('tedious').Request;
var config = require('./dbconfig');

var MSSQL = {
    procedures: {},

    getProcedures: function() {
        var conn = new Connection(config); 
        
        MSSQL.procedures = {};
        var name = '';
        var count = 0;
        
        conn.on('connect', function(err) {
            if(err) {
                console.log(err.message);
            }
            else {
                var request = new Request('getProcedures', function(err, rowCount, rows) {
                    if(err) {
                        console.log(err.message);
                    }

                    conn.close();

                }).on('row', function(cols) {
                    if(name !== cols[0].value) {
                        name = cols[0].value;
                        MSSQL.procedures[name] = [];
                    }
        
                    if(cols[1].value !== null) {
                        MSSQL.procedures[name] = JSON.parse(cols[1].value);
                    }
                })

                conn.execSql(request);
            }
        });
    }
}

module.exports = MSSQL;

